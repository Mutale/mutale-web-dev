from django.contrib import admin
from .models import NewsletterSignUp
from .form import NewsletterSignUpForm


class SignUpAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "timestamp", "updated"]
    form = NewsletterSignUpForm


admin.site.register(NewsletterSignUp, SignUpAdmin)
