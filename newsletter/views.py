from django.core.mail import send_mail
from django.shortcuts import render
from .form import NewsletterSignUpForm
from django.conf import settings


def contact(request):
    form = NewsletterSignUpForm(request.POST or None)
    if form.is_valid():
        firstname = form.cleaned_data.get("first_name")
        lastname = form.cleaned_data.get("last_name")
        email = form.cleaned_data.get("email")

        subject = "Contact Details"
        from_email = "peter@mutale.nl"
        to_email = "familie@mutale.nl"

        contact_message = "%s: %s via %s" % (
            firstname,
            lastname,
            email
        )
        send_mail(subject,
                  contact_message,
                  [to_email],
                  fail_silently=False
                  )
    context = {
            'form': form,
    }
    return render(request, "newsletter/home.html", context)
