from __future__ import unicode_literals

from django.db import models


class NewsletterSignUp(models.Model):
    first_name = models.CharField(max_length=128, blank=False, verbose_name='First Name')
    last_name = models.CharField(max_length=128, blank=False, verbose_name='Last Name',
                                 help_text='Family name')
    email = models.EmailField(verbose_name='Email')
    timestamp = models.DateField(auto_now_add=True, auto_now=False)
    updated = models.DateField(auto_now_add=False, auto_now=True)

    def __unicode__(self):  # __str__
        return self.first_name
